package org.lromero.strategictactoe;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * <h1>Tic-Tac-Toe Board</h1>
 * A JPanel containing nine tiles
 * @author lromero
 * @version 2.2
 */
public class Board extends JPanel
{
	/**
	 * Version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Array to store all the tiles present on the board
	 */
	private Tile[][] tiles; // Array to hold all tiles on the board
	
	/**
	 * The user-friendly name of this board which will be displayed in the border and in the game stats field
	 */
	private String boardTitle; // The title to be displayed with the board
	
	/**
	 * Constructs a new board
	 * @param boardTitle The board title to be displayed
	 * @param boardCode The two character code for the board
	 */
	public Board(String boardTitle, String boardCode)
	{
		this.boardTitle = boardTitle;
		this.setLayout(new GridLayout(3,3));
		this.setBorder(BorderFactory.createTitledBorder(boardTitle));
		this.setBackground(new Color(135, 206, 250));
		tiles = new Tile[3][3]; // Initialize tiles array
		this.initTiles();
		this.setBackground(new Color(135, 206, 250));
	} // end constructor
	
	/**
	 * Initialize all tiles being used on this board
	 */
	private void initTiles()
	{		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				tiles[i][j] = new Tile(String.valueOf(i) + String.valueOf(j));
				this.add(tiles[i][j]); // Add current tile to the panel
			}
		}
	} // end initTiles
	
	/**
	 * Gets the user-friendly name of the board
	 * @return Title of the board
	 */
	public String getTitle()
	{
		return boardTitle;
	} // end getTitle
	
	/**
	 * Sets all tiles on this board to enabled
	 */
	public void enableBoard()
	{
		for(Tile[] t1 : tiles)
		{
			for(Tile t2 : t1)
			{
				t2.setEnabled(true);
			}
		}
	} // end enable
	
	/**
	 * Sets all tiles on this board to disabled
	 */
	public void disableBoard()
	{
		for(Tile[] t1 : tiles)
		{
			for(Tile t2 : t1)
			{
				t2.setEnabled(false);
			}
		}
	} // end disable
	
	/**
	 * Returns the specified Tile object
	 * @param column Column index of the Tile
	 * @param row Row index of the Tile
	 * @return The Tile object at the given index
	 */
	public Tile getTile(int column, int row)
	{
		return tiles[column][row];
	} // end getTile
	
	/**
	 * Returns the winner of the board,  NONE if there is no winner, or TIED if the board is full with no winner
	 * @return Winner of the board
	 */
	public String status()
	{	
		// Check columns
		for(int i = 0; i < 3; i++)
		{
			/**
			 * Holds the last value that was read in from the boards
			 */
			String lastValue = Window.NONE;
			
			/**
			 * Counter for how many consecutive wins there are from the same player
			 */
			int consecutive = 0;
			
			for(int j = 0; j < 3; j++)
			{
				if(tiles[j][i].getMarker().equals(Window.NONE))
				{
					continue; // If the tile is not claimed, continue out of if else block
				}
				else if(lastValue.equals(Window.NONE))
				{
					lastValue = tiles[j][i].getMarker();
					consecutive++;
				}
				else if(tiles[j][i].getMarker().equals(lastValue))
				{
					consecutive++;
				}
				else
				{
					lastValue = tiles[j][i].getMarker();
					consecutive = 1; // Sets to 1 to indicate new player
				}
			}
			
			if(consecutive == 3) // If there are three claimed tiles by the same player in a row
			{
				if(lastValue.equals(Window.X_PLAYER))
				{
					markWinner(Window.X_PLAYER);
					return Window.X_PLAYER;
				}
				else
				{
					markWinner(Window.O_PLAYER);
					return  Window.O_PLAYER;
				}
			}
			
		} // end column check
		
		// Check rows
		for(int i = 0; i < 3; i++)
		{
			/**
			 * Holds the last value that was read in from the boards
			 */
			String lastValue = Window.NONE;
			
			/**
			 * Counter for how many consecutive wins there are from the same player
			 */
			int consecutive = 0;
			
			for(int j = 0; j < 3; j++)
			{
				if(tiles[i][j].getMarker().equals(Window.NONE))
				{
					continue;
				}
				else if(lastValue.equals(Window.NONE))
				{
					lastValue = tiles[i][j].getMarker();
					consecutive++;
				}
				else if(tiles[i][j].getMarker().equals(lastValue))
				{
					consecutive++;
				}
				else
				{
					lastValue = tiles[i][j].getMarker();
					consecutive = 1; // Sets to 1 to indicate new player
				}
			}
			
			if(consecutive == 3) // If there are three claimed tiles by the same player in a row
			{
				if(lastValue.equals(Window.X_PLAYER))
				{
					markWinner(Window.X_PLAYER);
					return Window.X_PLAYER;
				}
				else
				{
					markWinner(Window.O_PLAYER);
					return Window.O_PLAYER;
				}
			}
			
		} // end row check
		
		// Begin diagonal check
		if(tiles[0][0].getMarker().equals(tiles[1][1].getMarker()) && tiles[1][1].getMarker().equals(tiles[2][2].getMarker()) && !(tiles[0][0].getMarker().equals(Window.NONE)))
				{
					if(tiles[0][0].getMarker().equals(Window.X_PLAYER))
					{
						markWinner(Window.X_PLAYER);
						return Window.X_PLAYER;
					}
					else
					{
						markWinner(Window.O_PLAYER);
						return Window.O_PLAYER;
					}
				} // end diagonal check
		
		// Begin anti-diagonal check
		if(tiles[0][2].getMarker().equals(tiles[1][1].getMarker()) && tiles[1][1].getMarker().equals(tiles[2][0].getMarker()) && !(tiles[0][2].getMarker().equals(Window.NONE)))
		{
			if(tiles[0][2].getMarker().equals(Window.X_PLAYER))
			{
				markWinner(Window.X_PLAYER);
				return Window.X_PLAYER;
			}
			else
			{
				markWinner(Window.O_PLAYER);
				return Window.O_PLAYER;
			}
		} // end anti-diagonal check
		
		// Begin tie check
		for(Tile[] t1 : tiles)
		{
			for(Tile t2 : t1)
			{
				if(t2.getMarker().equals(Window.NONE))
				{
					return Window.NONE; // If at least a single tile is empty, there is no tie 
				}
			}
		} // end tie check
		
		markWinner(Window.TIED); // If above operations do not return, mark board as tied
		return Window.TIED; // If above operations do not return, then the game is tied
		
	} // end status
	
	/**
	 * Marks the board to indicate the player that claimed it, or if it is tied
	 * @param status The status that should be marked on the board
	 */
	public void markWinner(String status)
	{
		if(status.equals(Window.X_PLAYER)) // BLUE 'X'
		{
			for(int i = 0; i < 3; i++)
			{
				tiles[i][i].setBackground(Color.BLUE);
				tiles[i][2 - i].setBackground(Color.BLUE);
			}
		}
		else if(status.equals(Window.O_PLAYER)) // RED 'O'
		{
			tiles[0][1].setBackground(Color.RED);
			tiles[1][0].setBackground(Color.RED);
			tiles[1][2].setBackground(Color.RED);
			tiles[2][1].setBackground(Color.RED);
		}
		else if(status.equals(Window.TIED))
		{
			tiles[0][0].setBackground(Color.YELLOW); // YELLOW CORNERS
			tiles[2][0].setBackground(Color.YELLOW);
			tiles[0][2].setBackground(Color.YELLOW);
			tiles[2][2].setBackground(Color.YELLOW);
		}
	} // end markWinner
	
	
	/**
	 * Re-enables the board and resets all tiles on the board to the default marker, and clears and colors
	 */
	public void reset()
	{
		this.enableBoard(); // re-enable board
		for(Tile[] t1 : tiles)
		{
			for (Tile t2 : t1)
			{
				t2.reset();
			}
		}
	} // end reset
	
	/**
	 * Set all tiles in board to the states supplied
	 * @param boardTiles List of all states for tiles
	 */
	public void setTiles(String boardTiles)
	{
		String[] tilesStatus = boardTiles.split(","); // Split boardTiles into array
		int current = 0;
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				tiles[i][j].claim(tilesStatus[current]);
				current++;
			}
		}
		this.status(); // Refresh the board with loaded information
	} // end setTiles
	
	/**
	 * Dump all tile states into a String for saving
	 * @return String of all tile states
	 */
	@Override
	public String toString()
	{
		String states = ""; // States for all tiles will be appended to this
		
		for(Tile[] t1 : tiles)
		{
			for(Tile t2 : t1)
			{
				states += t2.toString() + ",";
			}
		}
		
		states = states.substring(0, states.length() - 1); // Remove comma from end
		
		return states;
	}// end toString
} // end Board
