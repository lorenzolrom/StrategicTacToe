package org.lromero.strategictactoe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * <h1>Strategic-Tac-Toe</h1>
 * An advanced game of Tic Tac Toe
 * <p>
 * ISTE 121 Mini-Project Group 24, Lorenzo Romero, Justin Stein
 * @author	lromero
 * @version	2.2
 */
public class Window extends JFrame
{
	/**
	 * Serial version number
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The user-friendly name for the 'X' player
	 */
	public static final String X_PLAYER = "Player X";
	
	/**
	 * The user friendly name for the 'Y' player
	 */
	public static final String O_PLAYER = "Player O";
	
	/**
	 * Internal designation for either a tile or board that has not been claimed or won
	 */
	public static final String NONE = "NONE";
	
	/**
	 * Internal designation for a tied board or game
	 */
	public static final String TIED = "TIED";
	
	/**
	 * An array that will hold nine Board objects (or all Board objects being used in the game)
	 */
	private Board boards[][];
	
	/**
	 * The JPanel that will be shown at the top of the window indicating current turn, current player, and current bord
	 */
	private StatBar sb;
	
	/**
	 * The player who is currently making the next turn
	 */
	private String currentPlayer; // Holds the current player of the game
	
	/**
	 * The last tile that was pressed, stored as a 2 digit code to reference the board it was going to.
	 */
	private String lastTileCode;
	
	/**
	 * Name of the save file
	 */
	private final String SAVE_FILE_NAME = "sttt.sav";
	
	private final int NEXT_BOARD_HEADER_INDEX = 0;
	private final int CURRENT_PLAYER_HEADER_INDEX = 1;
	private final int TURN_COUNTER_HEADER_INDEX = 2;
	
	/**
	 * The main method of the Window class calls the constructor, which moves the code into a non-static context
	 * @param args Command line arguments, which are unused for this program
	 */
	public static void main(String[] args)
	{
		new Window();
	} // end main method
	
	/**
	 * Constructs a new Window, while also creating Board objects, menu bar, and footer (some of this is done via other methods)
	 */
	public Window()
	{
		currentPlayer = "Player X"; // The game will always start with Player X going first
		boards = new Board[3][3]; // Initializes the array of Boards so it will have nine, arranged in 3x3 form
		
		// Define window parameters
		this.setTitle("Strategic Tac Toe");
		this.setSize(500, 500);
		this.setMinimumSize(new Dimension(500,500)); // Set minimum size to 500 x 500, it looks the best like this...
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLF();
		this.setIconImage(new ImageIcon(getClass().getResource("images/sttt.png")).getImage());
		this.getContentPane().setBackground(new Color(135, 206, 250));
		
		this.init(); // Initialize the game window
		
		// Display window 
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	} // end constructor
		
	/**
	 *Create the menu bar and the footer that will be displayed in the Window object
	 */
	private void init()
	{	
		/**
		 * The menu bar to be displayed at the top of the window
		 */
		JMenuBar menuBar = new JMenuBar();
				
		/**
		 * Footer that will be displayed at the bottom of the window
		 */
		JLabel credits = new JLabel("Strategic-Tac-Toe Version 2.2 | Lorenzo Romero, Justin Stein", JLabel.CENTER);
		
		/**
		 * Panel to hold all nine game boards in a 3x3 grid, with spacing of 10 between each
		 */
		JPanel main = new JPanel(new GridLayout(3,3, 10, 10));
		main.setBackground(new Color(135, 206, 250));
		
		this.initBoards(); // Initialize boards
		this.initMenuBar(menuBar);
		
		// Add boards to main
		for(Board[] b1 : boards)
		{
			for(Board b2 : b1)
			{
				main.add(b2); // Add individual board to main panel
			}
		}
		
		this.setJMenuBar(menuBar); // Add menu bar to JFrame
		this.add(main, BorderLayout.CENTER); // Add main to the center of the JFrame
		this.add(credits, BorderLayout.SOUTH); // Add credits to the footer of the JFrame
		
		// Initialize stat bar and place in the north area of the window
		sb = new StatBar();
		this.add(sb, BorderLayout.NORTH);
		
	} // end init
	
	/**
	 * Initialize the boards array, adds Action Listener to Tiles
	 */
	private void initBoards()
	{
		/**
		 * The number that will be shown with the name of the board displayed to the user in the game window
		 */
		int boardNameCounter = 1;
		
		// Initialize all boards in the array
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				boards[i][j] = new Board("Board " + String.valueOf(boardNameCounter),
						String.valueOf(i) + String.valueOf(j)); // Board code is the array index as a String
				boardNameCounter++; // Move to next number for the next board name
			}
		}
		
		/**
		 * Action Listener for the individual tiles
		 */
		TileListener tl = new TileListener();
		
		for(Board[] b1 : boards) // Cycle through boards
		{
			for(Board b2 : b1)
			{
				for(int i = 0; i < 3; i++) // Cycle through tiles
				{
					for(int j = 0; j < 3; j++)
					{
						b2.getTile(i, j).addActionListener(tl); // Add Action Listener
					}
				}
			}
		}
		
	} // end initBoards
	
	/**
	 * Initialize the JMenuBar
	 */
	private void initMenuBar(JMenuBar menuBar)
	{
		/**
		 * Menu item for restarting the game
		 */
		JMenuItem newGame = new JMenuItem("New Game");
		
		/**
		 * Menu item for exiting the program
		 */
		JMenuItem exitGame = new JMenuItem("Exit");
		
		/**
		 * Menu item for displaying program information to the user
		 */
		JMenuItem about = new JMenuItem("About");
		
		/**
		 * Menu item for displaying instructions about playing the game to the user
		 */
		JMenuItem howToPlay = new JMenuItem("How to Play");
		
		/**
		 * Menu item for saving the game
		 */
		JMenuItem saveItem = new JMenuItem("Save");
		
		/**
		 * Menu item for loading the game
		 */
		JMenuItem loadItem = new JMenuItem("Load");
		
		/**
		 * Menu that will contain options for restarting game and exiting game
		 */
		JMenu file = new JMenu("File");
		
		/**
		 * Menu that will contain options for information about the program and how to play
		 */
		JMenu help = new JMenu("Help");
		
		// Add menu items to their respective menu
		file.add(newGame);
		file.addSeparator();
		file.add(saveItem);
		file.add(loadItem);
		file.addSeparator();
		file.add(exitGame);
		
		help.add(howToPlay);
		help.add(about);
		
		// Add menus to menu bar
		menuBar.add(file);
		menuBar.add(help);
		
		/**
		 * Action Listener for menu operations, NOT including the reset function or save/load operations, handled below
		 */
		MenuListener ml = new MenuListener();
		
		/**
		 * Action Listener for save/load operations
		 */
		SaveListener sl = new SaveListener();
		
		/**
		 * Adds a special Action Listener for the reset option
		 */
		newGame.addActionListener(new ActionListener()
		{
			/**
			 * Handles the reset button action, which will call the reset method of the Window class
			 * @param e The ActionEvent passed by the menu item
			 */
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int quitExistingGame = JOptionPane.YES_OPTION;
				if (Integer.parseInt(sb.getTurns()) != 0)
				{
					quitExistingGame = JOptionPane.showConfirmDialog(null, "Do you want to quit your existing game?", "Confirm", JOptionPane.YES_NO_OPTION);
				}
				
				if (quitExistingGame == JOptionPane.YES_OPTION) { reset(); }
			}
		}
		);
		
		// Add remaining Action Listeners
		exitGame.addActionListener(ml);
		about.addActionListener(ml);
		howToPlay.addActionListener(ml);
		saveItem.addActionListener(sl);
		loadItem.addActionListener(sl);
	} // end initMenuBar
	
	/**
	 * Checks board information after game loaded from save file.
	 * @param gameInfo - The array of game info passed into the program from the file.
	 */
	private void processLoad(String[] gameInfo)
	{
		for (Board[] b1 : boards)
		{
			for (Board b2 : b1)
			{
				b2.status();
			}
		}
		
		if (this.status().equals(Window.NONE))
		{
			activateNextBoard(gameInfo[NEXT_BOARD_HEADER_INDEX]);
		}
		else // If game fails above condition, game should be terminated
		{
			endGame(this.status());
		}
		
		sb.setTurns(gameInfo[TURN_COUNTER_HEADER_INDEX]);
		this.currentPlayer = gameInfo[CURRENT_PLAYER_HEADER_INDEX];
		sb.setPlayer(currentPlayer);
	}
	
	/**
	 * Steps the game forward one turn, checking for winners and updating graphics if necessary
	 * @param boardCode Two character board code from the Tile that was last pressed
	 */
	private void stepGame(String boardCode)
	{
		// Force all boards to perform a status check
		for(Board[] b1 : boards)
		{
			for(Board b2 : b1)
			{
				b2.status();
			}
		}
		
		if(this.status().equals(Window.NONE)) // If the game has not been won, then set up for next move
		{
			nextPlayer(); // Sets game to next player
			activateNextBoard(boardCode); // Activate the next board
		}
		else // If game fails above condition, game should be terminated
		{
			endGame(this.status());
		}
	} // end stepGame
	
	/**
	 * Checks the entire game for a winner
	 */
	private String status()
	{
		// Check columns
		for(int i = 0; i < 3; i++)
		{
			/**
			 * Holds the last value that was read in from the boards
			 */
			String lastValue = Window.NONE; // Player who claimed last tile
			
			/**
			 * Counter for how many consecutive wins there are from the same player
			 */
			int consecutive = 0; // Number of consecutive claims
			
			// Begin column check
			for(int j = 0; j < 3; j++)
			{
				if(boards[j][i].status().equals(Window.NONE))
				{
					continue;
				}
				else if(lastValue.equals(Window.NONE))
				{
					lastValue = boards[j][i].status();
					consecutive++;
				}
				else if(boards[j][i].status().equals(lastValue))
				{
					consecutive++;
				}
				else
				{
					lastValue = boards[j][i].status();
					consecutive = 1; // Sets to 1 to indicate new player
				}
			}
			if(consecutive == 3) // If there are three claimed tiles by the same player in a row
			{
				if(lastValue.equals(Window.X_PLAYER))
				{
					return Window.X_PLAYER;
				}
				else if(lastValue.equals(Window.O_PLAYER))
				{
					return Window.O_PLAYER;
				}
			}
		} // end column check
		
		// Begin row check
		for(int i = 0; i < 3; i++)
		{
			String lastValue = Window.NONE; // Player who claimed last tile
			int consecutive = 0; // Number of consecutive claims
			for(int j = 0; j < 3; j++)
			{
				if(boards[i][j].status().equals(Window.NONE))
				{
					continue;
				}
				else if(lastValue.equals(Window.NONE))
				{
					lastValue = boards[i][j].status();
					consecutive++;
				}
				else if(boards[i][j].status().equals(lastValue))
				{
					consecutive++;
				}
				else
				{
					lastValue = boards[i][j].status();
					consecutive = 1; // Sets to 1 to indicate new player
				}
			}
			if(consecutive == 3) // If there are three claimed tiles by the same player in a row
			{
				if(lastValue.equals(Window.X_PLAYER))
				{
					return Window.X_PLAYER;
				}
				else if(lastValue.equals(Window.O_PLAYER))
				{
					return Window.O_PLAYER;
				}
			}
		} // end row check
				
		// Begin diagonal check
		if(boards[0][0].status().equals(boards[1][1].status()) && boards[1][1].status().equals(boards[2][2].status())
				&& !(boards[0][0].status().equals(Window.NONE) || boards[0][0].status().equals(Window.TIED)))
				{
					if(boards[0][0].status().equals(Window.X_PLAYER))
					{
						return Window.X_PLAYER;
					}
					else
					{
						return Window.O_PLAYER;
					}
				} // end diagonal check
		
		// Begin anti-diagonal check
		if(boards[0][2].status().equals(boards[1][1].status()) && boards[1][1].status().equals(boards[2][0].status())
				&& !(boards[0][2].status().equals(Window.NONE) || boards[0][2].status().equals(Window.TIED)))
				{
					if(boards[0][2].status().equals(Window.X_PLAYER))
					{
						return Window.X_PLAYER;
					}
					else
					{
						return Window.O_PLAYER;
					}
				} // end anti-diagonal check
		
		// Begin tie check
		for(Board[] b1 : boards)
		{
			for(Board b2 : b1)
			{
				if(b2.status().equals(Window.NONE))
				{
					return Window.NONE; // If at least a single tile is empty, there is no tie 
				}
			}
		} // end tie check
		
		return Window.TIED; // If above operations do not return, then the game is tied
	} // end isGameWinner
	
	/**
	 * Sets up game for the next round, disabling all boards and re-enabling all boards in play
	 */
	private void activateNextBoard(String board)
	{	
		disableAll(); // Disable all boards
		// Re-enable the next board
		
		/**
		 * Board object that references the next board being played, which is stored in the boards array
		 */
		Board nextBoard = boards[Integer.parseInt(board.substring(0, 1))][Integer.parseInt(board.substring(1, 2))];
		
		if(nextBoard.status().equals(Window.NONE)) // If board has no winner
		{
			nextBoard.enableBoard();
			sb.setBoard(nextBoard.getTitle()); // Change status bar to show what board should be used
		}
		else
		{
			for(Board[] b1 : boards)
			{
				for(Board b2 : b1)
				{
					if(b2.status().equals("NONE"))
					{
						b2.enableBoard();
					}
				}
			}
			
			sb.setBoard("Any Board"); // Set display to any board
		}
	} // end nextBoard
	
	/**
	 * Toggles between who is the current player
	 */
	private void nextPlayer()
	{
		if(currentPlayer.equals(Window.X_PLAYER))
		{
			currentPlayer = Window.O_PLAYER;
		}
		else
		{
			currentPlayer = Window.X_PLAYER;
		}
		
		sb.cyclePlayerText(); // Set next player
		sb.setTurns(sb.getTurns() + 1); // Sets text to the value of the current setting plus one
		
	} // end nextPlayer
	
	/**
	 * Set the appearance of the entire JFrame to the operating system's default UI
	 */
	private void setLF()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException | InstantiationException | IllegalAccessException	| UnsupportedLookAndFeelException e)
		{
			// Default UI is used
		}
	} // end setLF
	
	/**
	 * Resets the entire game, calling the reset method of all boards
	 */
	private void reset()
	{
		sb.reset(); // reset status bar
		currentPlayer = Window.X_PLAYER; // Reset player to X	
		for(Board[] b1 : boards)
		{
			for(Board b2 : b1)
			{
				b2.reset(); // Reset individual board
			}
		}
	} // end reset
	
	/**
	 * Ends the game and announces the winner
	 * @param winner The winner of the game
	 */
	private void endGame(String winner)
	{
		disableAll(); // Disable all boards
		
		if(winner.equals(Window.TIED))
		{
			JOptionPane.showMessageDialog(null, "Game Tied", "Game Over", JOptionPane.PLAIN_MESSAGE); // Displays game tied message
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Game Won by " + winner, "Game Over", JOptionPane.PLAIN_MESSAGE);
		}
	} // end endGame
	
	/**
	 * Disable all boards
	 */
	private void disableAll()
	{
		// Disable all boards
		for(Board[] b1 : boards)
		{
			for(Board b2 : b1)
			{
				b2.disableBoard();
			}
		}
	} // end disableAll
	
	/**
	 * Save all board states to a file
	 */
	private void save()
	{
		File saveFile = new File(SAVE_FILE_NAME); // Open file for saving
		
		try
		{
			PrintWriter pw = new PrintWriter(new FileWriter(saveFile));
			String gameInfo = lastTileCode + "," + currentPlayer + "," + sb.getTurns();
			pw.println(gameInfo);
			for(Board[] b1 : boards)
			{
				for(Board b2 : b1)
				{
					pw.println(b2.toString()); // Print board to new line
				}
			}
			
			pw.close(); // Close print writer
			
		}
		catch(IOException e)
		{
			JOptionPane.showMessageDialog(null, "Game Save Failed", "Save Game", JOptionPane.ERROR_MESSAGE);
		}
	} // end save
	
	/**
	 * Sets all boards to states from file
	 */
	private void load()
	{
		//Check if there is already a game in progress.
		int quitExistingGame = JOptionPane.YES_OPTION;
		if (Integer.parseInt(sb.getTurns()) != 0)
		{
			quitExistingGame = JOptionPane.showConfirmDialog(null, "Do you want to quit your existing game?", "Confirm", JOptionPane.YES_NO_OPTION);
		}
		
		//Execute the code if there is no game in progress or if the user wants to overwrite the game.
		if (quitExistingGame == JOptionPane.YES_OPTION)
		{
			this.reset(); // Reset current game to clear board for loaded
			
			File loadFile = new File(SAVE_FILE_NAME); // Open file to load
			
			Scanner in = null;
			
			try
			{
				in = new Scanner(loadFile);
				String[] gameHeader = in.nextLine().split(",");
				while(in.hasNextLine())
				{
					for(Board[] b1 : boards)
					{
						for(Board b2 : b1)
						{		
							b2.setTiles(in.nextLine()); // Load in next line and set tiles
						}
					}
				}
				
				this.status(); // Refresh game with loaded data
				this.processLoad(gameHeader);
			}
			catch(IOException e)
			{
				JOptionPane.showMessageDialog(null, "Game Load Failed", "Load Game", JOptionPane.ERROR_MESSAGE);
			}
			finally
			{
				in.close(); // Close scanner
			}
		}
	} // end load
	
	/**
	 * Handles save/load operations
	 * @author lromero
	 */
	class SaveListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			switch(e.getActionCommand())
			{
				case "Save":
					save();
					break;
				case "Load":
					load();
					break;
			}
		}
	}
	
	/**
	 * Handles when a player clicks on a tile
	 * @author lromero
	 */
	class TileListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			/**
			 * Tile object that references a cast object of the source of the ActionEvent
			 */
			Tile source = (Tile)e.getSource();
			if(source.claim(currentPlayer)) // If claim was successful
			{
				lastTileCode = source.getCode();
				stepGame(source.getCode()); // Moves the game forward using the code of the checked tile
			}
			// Otherwise game does nothing and waits for a valid input
		}
	} // end inner class TileListener
	
} // end Window

/**
 * Panel containing information about the current game turn, player, and active board(s)
 */
class StatBar extends JPanel
{
	/**
	 * Version ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Displays a numerical representation of the current turn as a String, initially zero
	 */
	private JTextField turnCounter = new JTextField("0", 3);
	
	/**
	 * Displays the user-friendly name of the current player
	 */
	private JTextField currentPlayer = new JTextField(Window.X_PLAYER, 10);
	
	/**
	 * Displays the board (or boards) that can be used for the turn
	 */
	private JTextField currentBoard = new JTextField("Any Board", 10);
	
	/**
	 * Create new StatBar
	 */
	public StatBar()
	{
		this.setBackground(new Color(192, 192, 192));
		// Set all text fields so they cannot be edited
		turnCounter.setEditable(false);
		currentPlayer.setEditable(false);
		currentBoard.setEditable(false);
		
		// Add text fields to this panel
		this.add(new JLabel("Turn: "));
		this.add(turnCounter);
		this.add(new JLabel("Player: "));
		this.add(currentPlayer);
		this.add(new JLabel("Use: "));
		this.add(currentBoard);
	} // end StatBar constructor
	
	/**
	 * Toggles what player is displayed as active
	 */
	public void cyclePlayerText()
	{
		if(currentPlayer.getText().equals(Window.X_PLAYER))
		{
			currentPlayer.setText(Window.O_PLAYER);
		}
		else
		{
			currentPlayer.setText(Window.X_PLAYER);
		}
	} // end cyclePlayerText
	
	/**
	 * Sets the player based on the parameter passed in.
	 * @param player - The next player.
	 */
	public void setPlayer(String player)
	{
		if(player.equals(Window.X_PLAYER))
		{
			currentPlayer.setText(Window.X_PLAYER);
		}
		else if(player.equals(Window.O_PLAYER))
		{
			currentPlayer.setText(Window.O_PLAYER);
		}
	} // end setPlayer
	
	/**
	 * Sets the turnCounter text field number of turns.
	 * @param turns - The number of turns to set.
	 */
	public void setTurns(String turns)
	{
		this.turnCounter.setText(turns);
	} // end setTurns
	
	/**
	 * Returns the number in the turn counter text field.
	 * @return The number in the turn counter text field.
	 */
	public String getTurns()
	{
		return this.turnCounter.getText();
	}
	
	/**
	 * Sets the next board field to the board that is being used for the next turn
	 * @param board Board being used in the next turn
	 */
	public void setBoard(String title)
	{
		currentBoard.setText(title); // Sets text to title of the board
	} // end setBoard
	
	/**
	 * Reset all text fields to default values
	 * <p>
	 * Resets turnCounter to zero, currentPlayer to Player X, and currentBoard to any
	 */
	public void reset()
	{
		turnCounter.setText("0");
		currentPlayer.setText(Window.X_PLAYER);
		currentBoard.setText("Any Board");
	} // end reset
} // end StatBar

/**
 * Handles menu operations with the exception of reset
 * @author lromero
 */
class MenuListener implements ActionListener
{
	@Override
	public void actionPerformed(ActionEvent e)
	{
		switch(e.getActionCommand())
		{
			case "Exit": // Closes program
				int quitExistingGame = JOptionPane.showConfirmDialog(null, "Are you sure you wish to exit?", "Confirm", JOptionPane.YES_NO_OPTION);
				if (quitExistingGame == JOptionPane.YES_OPTION) { System.exit(1); }
				break;
			case "About":
				JOptionPane.showMessageDialog(null, "Strategic-Tac-Toe\n" + "Version 2.2" + "\nLorenzo Romero, Justin Stein", "Strategic-Tac-Toe", JOptionPane.PLAIN_MESSAGE);
				break;
			case "How to Play":
				// Display JOptionPane with help dialog
				JOptionPane.showMessageDialog(null, "Rules:\n"
						+ "1) First player selects tile on any board\n"
						+ "2) Second player will select tile on board corresponding to tile picked in previous turn\n"
						+ "3) Players will take turns, changing boards to correspond with the relative location of the last tile picked\n"
						+ "4) If a player wins a board, that board is claimed for the player\n"
						+ "5) The first player to get three BOARDS in a row is the winner\n"
						+ "If a board is selected that is already claimed, all non-claimed boards will open up", "How to play Strategic Tac Toe", JOptionPane.PLAIN_MESSAGE);
				break;
		} // end switch
	} // end actionPerformed
} // end MenuListener