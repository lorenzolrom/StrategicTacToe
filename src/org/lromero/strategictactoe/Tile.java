package org.lromero.strategictactoe;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * <h1>Tic-Tac-Toe Board Tile</h1>
 * A JButton that can be claimed during a game of tic-tac-toe
 * @author lromero
 * @version 2.2
 */
public class Tile extends JButton
{
	/**
	 * Version ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Image for no player
	 */
	ImageIcon nImage;
	
	/**
	 * Image for the X player
	 */
	ImageIcon xImage;
	
	/**
	 * Image for the O player
	 */
	ImageIcon oImage;
	
	/**
	 * A two character String representing where on the Board this tile is located
	 */
	private String tileCode;
	
	/**
	 * Internal tracker for the player (or lack of) that claims the tile
	 */
	private String tileMarker;
	
	/**
	 * Constructs a new tile, passing the name to the JButton constructor and setting instance variable tileCode
	 * @param text Default String to display on button
	 * @param tileCode A two character String representing the Tiles location on the board
	 */
	public Tile(String tileCode)
	{
		tileMarker = Window.NONE; // Sets the marker for the tile to blank
		this.tileCode = tileCode;
		nImage = new ImageIcon(getClass().getResource("images/n.png"));
		xImage = new ImageIcon(getClass().getResource("images/x.png"));
		oImage = new ImageIcon(getClass().getResource("images/o.png"));
		this.setIcon(nImage); // Set image to the default none
	} // end constructor
	
	/**
	 * Claims this button for the given player
	 * @param player The player who has clicked on this tile
	 * @return Did the tile claim operation succeed?
	 */
	public boolean claim(String player)
	{
		if(!(tileMarker.equals(Window.NONE)))
		{
			return false; // Tile already claimed, operation failed
		}
		else if(player.equals(Window.X_PLAYER))
		{
			this.setIcon(xImage);
			tileMarker = Window.X_PLAYER;
		}
		else if(player.equals(Window.O_PLAYER))
		{
			this.setIcon(oImage);
			tileMarker = Window.O_PLAYER;
		}
		else
		{
			this.setIcon(nImage);
			tileMarker = Window.NONE;
		}
		return true;
	} // end claim
	
	/**
	 * Gets the current marker of the player claiming the tile
	 * @return Marker of the tile
	 */
	public String getMarker()
	{
		return tileMarker;
	} // end getMarker
	
	/**
	 * Set the state of the tile
	 * @param player The player that has this tile claimed in a loaded game
	 */
	public void setMarker(String player)
	{
		if(player.equals(Window.X_PLAYER)) // Compare supplied player to global variables in Window
		{
			this.setIcon(xImage); // Update icon image
			tileMarker = Window.X_PLAYER; // Update marker
		}
		else if(player.equals(Window.O_PLAYER))
		{
			this.setIcon(oImage);
			tileMarker = Window.O_PLAYER;
		}
		else
		{
			this.setIcon(nImage);
			tileMarker = Window.NONE;
		}
	}
	
	/**
	 * Gets the tile code
	 * @return Two character tile code
	 */
	public String getCode()
	{
		return tileCode;
	} // end getCode
	
	/**
	 * Resets the tile to the default state
	 */
	public void reset()
	{
		tileMarker = Window.NONE;
		this.setIcon(nImage);
		this.setBackground(null); // Reset any background color
	} // end reset
	
	/**
	 * Return state of the tile as a String
	 * @return String representing the claim state of the tile
	 */
	@Override
	public String toString()
	{
		if(this.getIcon() == this.xImage)
		{
			return Window.X_PLAYER;
		}
		else if(this.getIcon() == this.oImage)
		{
			return Window.O_PLAYER;
		}
		else
		{
			return Window.NONE;
		}
	}
} // end Tile class
