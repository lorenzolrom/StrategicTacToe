<h2>Strategic Tac Toe</h2>

<p>This is a project for ISTE-121 (Computer Problem Solving in the Info Domain II).
The instructions are as follows:</p>
<ol>
    <li>First player selects tile on any board.</li>
    <li>Second player will select tile on board corresponding to tile picked in previous turn.</li>
    <li>Players will take turns, changing boards to correspond with the relative location of the last tile picked</li>
    <li>First player to get Tic-Tac-Toe on any board is the winner.</li>
</ol>

<p>We hope you enjoy this game, and that it proves challenging yet fun!</p>